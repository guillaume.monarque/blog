---
title: Building a full-stack ecommerce website
date: 2022/04/06
tags: freelance‏‏‎ bigcommerce golang automation
description: This article is the feedback of a freelance mission I carried out during the last 8/9 months.
id: 1
---
_This article is the feedback of a freelance mission I carried out during the last 8/9 months._
_The client has allowed me to mention the business name, but I prefer not to mention them for the moment._

### The mission
I was contacted by the owner of a company based very close to where I live. The owner and I have a common friend, and one thing leading to another, we were put in contact. This company sells sporting goods (skateboards, scooters, etc.), clothings, safety gear (helmets, knee pads, etc.) and other things related to their activity. They sell both in their physical store & on their online store. Their online store is about 5 years old, and the client experienced a significant decline in online sales.
The client was looking for a freelance contractor to update the website and implement SEO good practices.
After having discussed with the client, we agreed for me to realize an assessment of the website. Here are the main elements I could idenfity:
- The website was based on Prestashop and self-hosted at a major hosting company
- The frontend Prestashop template was somewhat responsive, but easily usable on a small screen / smartphone
- The overall frontend website was outdated
- There isn't any employee / contractor tasked to maintain the Prestashop instance (no regular updates, etc.)
- A freelance developer was tasked to automate the products listing by scraping the different suppliers. A scraping & importing pipeline was running on a separate server, and the products were synced with the suppliers stock every 6 hours.

After having conducted this initial assessment, I had a meeting with the client where I exposed the problems I identified.
The client was right concerning the bad SEO performances of the website, but the bad performances were not the direct cause of the decline in sales. Here is a more detailed list of what was causing this decline:

- Prestashop
    - Outdated version running
    - Outdated frontend theme/template
        - Bad SEO practices
        - Bad mobile interface
    - No technical support
        - Growing list of security concerns
        - No one dared to update it, because previous update broke Prestashop
        - No backups
    - Self-hosted
        - No technical support (or expensive TS from hosting company)
    - Overall website is slow
        -More than 8000 products
        -Frontend takes 6/7 seconds to load
        -Back-office sometimes hangs for 5+ minutes
        -Images compression is not efficient / missing
- Products scraper / importer
    - The importer deletes products instead of deactivating them
        - Prestashop cache is always regenerating
        - Google bot is not happy with missing products pages
    - The importer generated a .csv file, which is then uploaded by hand in a custom-made Prestashop module

I explained to the client that the return on investment for any work done on the current website would be close to zero, if not negative. While the client was not ready to invest now in a new website, it was clear that things would only go downhill from now. The following mission contract was then signed between me and the client:

- Assess technical solutions and propose a new ecommerce stack (full-stack)
- Create and develop the new website (full-stack)
- Create a new products scraper / importer for the new website

I will not go into details related to the price of this mission, the client/contractor meetings planning, etc. and only focus on the technical part.

### Choosing a new ecommerce platform

It's clear for me now that Prestashop needs to go and be replaced.
There are a lot of options available and they can be split in two main categories: headless platforms and monolithics platforms.
The "head" is the frontend application, it's what the end-user sees and uses to purchase something from an online store.

