---
title: How to use Vite.js on Gitpod.io
date: 2021/08/24
tags: vitejs nodejs webdev sveltekit gitpod
description: This blog was recently (re)made from scratch using sveltekit. Vite is used unther the hood to add HMR capabilities.
id: 1003
---

This blog was recently (re)made from scratch using sveltekit. Vite is used unther the hood to add HMR capabilities.
The problem is that I mostly use gitpod as a dev. environnement because it enables me to work from any computer I may encounter (digital nomad flex).

Gitpod exposes any port your app may expose, except ones like 443, etc. Since gitpod is a web app, they do a little trick to expose ports by exposing them to a URL defined as follow: 
```language
https://EXPOSED_PORT-your-gitpod.gitpod.io
```
In my case, the vite wss endpoint is served at https://24678-my-gitpod.ws-eu16.gitpod.io/.

The trick is simply to define a custom wss server and clientPort in the Vite configuration. You have to create a vite.config.js inside your project root. If you're using svelteKit, you can't do that, and you must configure vite in your svelte.config.js file. This is the config I use in my svelte.config.js file:

``` typescript
import sveltePreprocess from "svelte-preprocess";
import static_adapter from "@sveltejs/adapter-static";
/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    /*     paths: {
      base: "/blog",
    }, */
    adapter: static_adapter(),
    // hydrate the <div id="svelte"> element in src/app.html
    target: "#svelte",
    vite: {
      server: {
        hmr: {
            protocol: "wss", 
            host: "24678-master-flex-s567sz.ws-eu17.gitpod.io/", 
            clientPort: "443", 
        },
    },
      optimizeDeps: {
        include: ["highlight.js/lib/core"],
      },
    },
  },
  preprocess: sveltePreprocess(),
};

export default config;
```




