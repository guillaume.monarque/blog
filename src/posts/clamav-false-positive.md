---
title: ClamAV false positive Archive.Test.Agent2-9953724-0
date: 2022/04/24
tags: clamav bug
description: It appears that the latest ClamAV database is broken and return a false positive when scanning a .zip file.
id: 1001
---

It appears that the latest ClamAV database is broken and return a false positive when scanning a .zip file.
If you're here, you may have encountered the following error/alert:
```bash
Archive.Test.Agent2-9953724-0
``` 

Don't panic, this is a false positive and you're not the first one who came across this issue. It seems that quite a few pipelines are being broken by this, and some DevSecOps engineers are getting angry.


The best thing to do right now is to wait for new database or a fix to be released, in a few hours at most.
You can also reverse to the previous ClamAV signatures definition database if you can't wait for the fix, but this is not the recommended solution.

In any case, I'll update this post if I have new information.

Update: The false positive has been fixed by ClamAV. Running freshclam should fix the problem!