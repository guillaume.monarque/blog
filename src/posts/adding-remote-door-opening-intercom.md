---
title: Hacking my intercom with remote door opening functionality, and more
date: 2019/11/08
tags: diy raspberrypi python hacking
description: I live in a flat, on the third floor, and I have a problem. When I'm not home (which is the case all the time, except at night and on sundays), I can't open the front door of my building when the delivery man needs to deposit a package in my letter box.
id: 1000
---

_Big thanks to [Clément](https://github.com/clementblaise) for helping me do this and making the python script_
_This was featured on [hackaday](https://hackaday.com/2019/12/02/quick-and-dirty-operate-an-intercom-via-telegram/)_

### The problem

I live in a flat, on the third floor, and I have a problem: when I'm not home (which is the case all the time, except at night and on sundays), I can't open the front door of my building when the delivery man needs to deposit a package in my letter box. The entry to my building is based on Vigik technology (NFC), but for some reason, the master pass used by delivery men doesn't work in my building. So what happens is that they call me when they can't enter, and since I'm at work, I must run all over town to get my packages.  
This is not acceptable, something must be done!

### The solution

I need to be able to remotely open the front door of my building. When I'm home, I can physically press the "open door" button of my intercom. But when I'm away, I need to be able to press this button anyway!

Before I go any further, I don't own this flat nor the building and I can't do what I want. I'm going to try to keep the intercom in it's original states, and do as few modifications and hardware mods as I can. The goal is to be able to reverse the intercom to the way it was when I'll leave this flat.

Let's get to it. This is my intercom:

<img src="/images/75024198_770400410143738_7470855028798914560_n.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

It has got two buttons as you can see: the top one opens the door of the building, and the other one doesn't do anything.  
Now, let's see what's inside.

<img src="/images/75339475_2499778803639702_4793102318864171008_n.jpg" width="250"style="border-radius:10px;margin-left:auto;margin-right:auto;"/>


Well, look at this bad boy. This intercom is maybe 30 years old, and the electronic inside is very basic.
The references I can read are **Elvox 166/6 ART.870/081**. This seems to be an Italian brand, since all forum posts about it are in Italian. After some googling, I found this:

![](/images/schema.png)

I'm not an electrical engineer, but this is a very basic wiring diagram. I don't want to understand everything, only where is the door opening button connected.

The intercom has four switches according to this diagram. My guess is that 1 and 2 are for the phone, since they are "connected" (one cannot be activated without the other), maybe for audio and voice, and the door must be connected to switch seven since it's connected to a 12V Lock. The intercom seems to be working with DC +12V. Art. 930 must be the intercom "server", connecting all intercoms.

After some measuring, it seems that this diagram doesn't match with my intercom. Here is what I found:

- Connecting pins 3 and 4 opens the door
- There's +13V between 2 and 6, I'll try to power the Raspberry with this

Here is a better view of the intercom circuit-board:  
<img src="/images/76194099_2499778986973017_5995801755697283072_n.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>
Now that I've identified the "pins" of the switch, I'll add two cables connected to 3 and 4. It now looks like this:  
<img src="/images/74571217_791549057962082_4474821224773451776_n.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

I have what I needed concerning the functionning of the intercom. Now, let's see what hardware I need.

### Parts list

For this project, I'm gonna do the obvious choise of using a Raspberry Pi. I also need something to close the circuit between 3 and 4 and simulate a button press: a relay. That's it!
I'll use a Telegram bot to trigger the relay and open the door.

Here is a more complete parts list:

- Raspberry Pi Zero with Wifi
- A Relay. I bought this 2 way relay on[ Amazon](https://www.amazon.fr/AptoFun-Optokoppler-Arduino-Raspberry-channels/dp/B01I41B7US/ref=sr_1_6), it was the smallest I could find.
- A basic soldering kit
- A multimeter
- A breadboard and some jumper cables

Here is the first prototype, I'm switching on a LED powered by an Arduino Uno.
<img src="/images/maker2.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

The raspberry is running the [python telegram bot](https://github.com/python-telegram-bot/python-telegram-bot), and it's operating the relay. Here is a bigger picture:

<img src="/images/maker.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

I soldered the necesserary pin headers on the Raspberry Pi Zero myself.

The relay is connected to +5V and Ground of the Raspberry Pi, and it's trigerred by GPIO 18.

Let's test it! In the picture below, the raspberry is powered by a phone charger, but I plan to use the +12V of the intercom to power the raspberry.

<img src="/images/73390699_572158276923401_8718441173137489920_n.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

After some debugging, everything works! When I send the open command to the telegram bot, the door opens. The last part is to fit everything into the intercom case. Hopefulle, the case has got a lot of spare room. I made a tiny voltage divider in order to power the raspberry with 5V.
<img src="/images/75627441_771352076670522_1922017707708907520_n.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

<img src="/images/75322575_489481741668324_785814092718800896_n.jpg" width="250" style="border-radius:10px;margin-left:auto;margin-right:auto;"/>

The code for the python telegram bot is:

```python

GPIO_PIN = 2

 def terminateProcess(signalNumber, frame):
     print('Cleaning GPIO')
     GPIO.cleanup()
     sys.exit(0)

 def openDoor(update, context):
     GPIO.output(GPIO_PIN, GPIO.LOW)
     context.bot.send_message(chat_id=update.message.chat_id, text='Entre poto')
     time.sleep(3)
     GPIO.output(GPIO_PIN, GPIO.HIGH)


 def ioSetup():
     GPIO.setmode(GPIO.BCM)
     GPIO.setwarnings(False)
     GPIO.setup(GPIO_PIN,GPIO.OUT)
     GPIO.output(GPIO_PIN, GPIO.HIGH)

 def main():
    ioSetup()
     signal.signal(signal.SIGINT, terminateProcess)
     updater = Updater(token=' :D ', use_context=True)
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
     level=logging.INFO)
     dispatcher = updater.dispatcher
     dispatcher.add_handler(CommandHandler('open', openDoor))
     updater.start_polling()
     updater.idle()


 if __name__ == '__main__':
     main()

```

This code runs on raspberry startup.
Thanks for reading :)
