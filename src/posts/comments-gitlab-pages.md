---
title: Comments! Adding comments to my gitlab pages site
date: 2022/06/26
tags: comments gitlab disqus integration sveltekit graphcomment
description: I'll talk about the very short journey of adding comments to my website's blog posts
id: 1004
---

As of today, it's now possible to write and publish comments on each of my blog posts. This website is hosted on gitlab pages; it's statically built and there is no backend server running, nor any database. In this context, having a comments system implies outsourcing the commenting logic to a third-party if I want to keep using gitlab pages.

I've came across a lot of cool self-hosted solutions, but they're sadly not an option for me.
After intensive research that lasted at least a whole minute, I narrowed down my options to two services:

- disqus
- graphcomment

### Disqus: love it or hate it

They are the leader in this market and, as far as I'm concerned, the go-to platform when you need to add a commenting system.
I've read good things about disqus, and a lot of bad things. I never fully make a judgement based on other people's opinions, but it's always something to take into account.
The article that best summurizes the critics I read about disqus is [this one](https://fatfrogmedia.com/delete-disqus-comments-wordpress/).

I visited a few blogs using disqus, and the page load speed was dramatically increased because of disqus. The ads displayed by disqus were also ternishing the "personal blog" experience, where you don't expect to see ads.

### Graphcomment: good enough for me

I don't want to spend too much time adding a commenting system to a blog no-one reads, let alone comments. I went with graphcomments, and the impact on the performances of my blog are not noticeable. The ol'reliable chrome lighthouse report went from a score of 99 to 96. Of course this is not the correct way to measure a website performances, but it's something.
The graphcomment admin panel is really not user friendly, I hope I won't have to administer a lot of comments. (I won't).
Integrating their commenting system is quite easy, I made a svelte component called comments.svelte, and it looks like this:

```typescript

<script>
  import { onMount } from "svelte";

  let id;
  var __semio__params = {
    graphcommentId: "gmsec", // make sure the id is yours

    behaviour: {
      uid: id,
    },
  };

  function __semio__onload() {
    __semio__gc_graphlogin(__semio__params);
  }

  onMount(async () => {
    var gc = document.createElement("script");
    gc.type = "text/javascript";
    gc.async = true;
    gc.onload = __semio__onload;
    gc.defer = true;
    gc.src =
      "https://integration.graphcomment.com/gc_graphlogin.js?" + Date.now();
    (
      document.getElementsByTagName("head")[0] ||
      document.getElementsByTagName("body")[0]
    ).appendChild(gc);
  });
</script>

<div id="graphcomment" />

```

This is called from the blog post page simply by including `<Comments id={post.id} />`.
Each post has its own id, and the id is passed to the comments component. Without this, every post would share the same comment thread instead of having its own.

I hope this is one day useful to someone looking to add comments to a static site and not wanting to use disqus.
Don't hesitate to leave a comment :)
