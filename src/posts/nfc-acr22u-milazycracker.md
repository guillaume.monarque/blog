---
title: NFC, ACR122U et compagnie
date: 2019/11/07
tags: nfc acr122u miLazyCracker café
description: On va s'intéresser à l'utilisation d'un lecteur NFC ACS ACR122U-9 sur Debian 10.
id: 1002
---

Bonjour!

On va s'intéresser à l'utilisation d'un lecteur NFC ACS ACR122U-9 sur Debian 10.

Si vous êtes ici, vous avez surement cherché sur Google comment solutionner une des erreurs suivantes:

- error libnfc.driver.acr122_usb Invalid RDR_to_PC_DataBlock
- error libnfc.driver.acr122_usb Unable to write to USB (Connection timed out)
- error libnfc.driver.acr122_usb Unable to claim USB interface (Permission denied)

Et vous n'avez surement pas réussi à vous en débarasser. Si vous avez récemment acheté votre lecteur sur Amazon, c'est normal. Je n'ai pas d'explication, mais certains lecteurs marchent, d'autre ne marchent pas. Après m'être arraché les cheveux et avoir creusé partout, j'ai trouvé la solution en modifiant quelques lignes dans les sources de libnfc.

Pour installer cette version modifiée de libnfc et voir si cela règle votre problème, suivez le guide!

Tout d'abord, désinstallez libnfc, et les paquets qui ont libnfc comme dépendance.

> sudo apt-purge libnfc\*

Maintenant, on va installer et compiler une version de libnfc modifiée par mes soins afin qu'elle fonctionne correctement avec les dernières versions du lecteur:

> sudo apt update && sudo apt upgrade  
> sudo apt install autoconf libglib2.0-dev  
> git clone https://github.com/apyredev/libnfc.git  
> cd libnfc  
> autoreconf -vis  
> ./configure
> make  
> sudo make install

Une fois libnfc installé, il y a quelques détails à régler.

> sudo nano /etc/modprobe.d/blacklist-libnfc.conf

Ajoutez "pn533_usb" à ce fichier.

Ensuite:

> modprobe -r pn533_usb  
> modprobe -r pn533  
> modprobe -r nfc  
> systemctl stop pcscd.socket  
> systemctl stop pcscd.service

Il est temps de voir si le lecteur marche!

> sudo nfc-list

C'est bon? Super! Si vous voulez utiliser miLazyCracker, alors il vous faut installer mfoc depuis les sources et non depuis apt. Sinon, vous ne pourrez pas utiliser ma version de libnfc.

Voici comment installer mfoc (hardnested) rapidement:

> sudo apt install liblzma\*  
> git clone https://github.com/vk496/mfoc  
> cd mfoc/  
> git checkout hardnested  
> autoreconf -vis  
> ./configure  
> make  
> sudo make install  
> cd src  
> sudo cp -a mfoc /usr/local/bin

A vous de jouer! Je ne recommande pas d'utiliser le script d'installation de miLazyCracker.  
Pour utilisez miLazyCracker et la fonction hardnested attack de mfoc, il vous manque un outil qui est crypto1_bs.  
J'ai préparé un dossier avec tous les fichiers nécessaires disponible sur cette page: téléchargez [crypto1_bs.zip](/download/crypto1_bs.zip) puis faites:

> unzip crypyo1_bs.zip  
> cd crypto1_bs  
> make clean  
> make  
> sudo cp -a libnfc_crypto1_crack /usr/bin

Vous pouvez désormais lancer miLazyCracker avec sudo ./miLazyCracker. Petit rappel: il est évident que vous ne devez "jouer" qu'avec des tags NFC vous appartenants.

Bonus: ajoutez ce fichier dans le répertoire de miLazyCracker, au même emplacement que le script: https://github.com/ikarus23/MifareClassicTool/blob/master/Mifare%20Classic%20Tool/app/src/main/assets/key-files/extended-std.keys
