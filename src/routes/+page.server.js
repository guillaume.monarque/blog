import fs from "fs";
/** @type {import('./$types').PageServerLoad} */
export function load() {
    const files = fs.readdirSync(`static/images/memes/`);
    const memes = [];
    files.forEach((file) => {
        memes.push({
            file,
        });
    });
    return {
        body: {
            memes: memes,
        },
    };
}