import showdown from "showdown";
import fs from "fs";
import showdownHighlight from "showdown-highlight";
showdown.extension("remove-p-from-img", function () {
  return [
    {
      type: "output",
      filter: function (text) {
        text = text.replace(
          // match all <p>'s before and after an <img> tag
          /(<\/?p[^>]*>)(?=<img.+>)|(<\/?p[^>]*>)(?<=<img.+>)/g,
          ""
        );

        return text;
      },
    },
  ];
});

/** @type {import('./$types').PageServerLoad} */
export function load({ params }) {
  const { slug } = params;
  const converter = new showdown.Converter({
    extensions: [showdownHighlight({
      pre: true
    }), "remove-p-from-img"]
  });
  converter.setFlavor('github');
  converter.setOption('parseImgDimensions', 'true');
  converter.setOption('simpleLineBreaks', 'true');


  const md = fs.readFileSync(`src/projects/${slug}.md`, {
    encoding: "utf8",
    flag: "r",
  });

  const mdarr = md.split("\n");
  const mdutil = md.replace(new RegExp(`(?:.*?\n){${5}}(?:.*?\n)`), "");
  const html = converter.makeHtml(mdutil);
  return {
    body: {
      html: html,
      title: mdarr[1].split(":")[1].trim(),
      date: mdarr[2].split(":")[1].trim(),
      tags: mdarr[3].split(":")[1].trim().split(" "),
      description: mdarr[4].split(":")[1].trim(),
      id: mdarr[5].split(":")[1].trim(),
    },
  };
}
