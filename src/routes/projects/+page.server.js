import fs from "fs";
/** @type {import('./$types').PageServerLoad} */
export function load() {
  const files = fs.readdirSync(`src/projects/`);
  const posts = [];
  files.forEach((file) => {
    const data = fs
      .readFileSync(`src/projects/${file}`, {
        encoding: "utf8",
        flag: "r",
      })
      .split("\n");
    const date = new Date(data[2].split(":")[1].trim());

    posts.push({
      title: data[1].split(":")[1].trim(),
      date: data[2].split(":")[1].trim(),
      tags: data[3].split(":")[1].trim().split(" "),
      description: data[4].split(":")[1].trim(),
      id: data[5].split(":")[1].trim(),
      slug: file.slice(0, -3),
      dateParsed: date,
    });
  });

  const postsSorted = posts.sort((a, b) => b.dateParsed - a.dateParsed);

  return {
    body: {
      posts: postsSorted,
    },
  };
}
