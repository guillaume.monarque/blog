import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: vitePreprocess(),
  kit: {
    adapter: adapter({
      pages: 'build',
      assets: 'build',
      fallback: null,
      precompress: false,
      strict: true,
      trailingSlash: 'always',

    }),
  },
  prerender: {
    // This can be false if you're using a fallback (i.e. SPA mode)
    default: true
  }
};

export default config;